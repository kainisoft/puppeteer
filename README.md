### Installation
1. git clone https://gitlab.com/kainisoft/puppeteer.git
2. ./install
3. source ~/.bashrc
4. npm run test

### How to write test
1. Copy file test.example.js and put into folder /stc/__tests
2. Rename copied file. Name should describe testing feature, for example "submit-order.js"
3. Follow instructions in file
3. Command to run test for copied file: npm run test submit-order 


**More Resources:**

https://pptr.dev/<br>
https://jestjs.io/en/
