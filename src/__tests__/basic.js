const devices = require('puppeteer/DeviceDescriptors');
const mainUrl = 'https://colony.build.wp.incentient.org/tablet';
const passcode = '1234';
const testRoomName = 'zincentienttestroom';
const timeout = 60000;

describe('Basic Test Script Boilerplate', () => {
  let page

  /**
   * Client Activation process.
   * Runs once before any test in this file.
   */
  beforeAll(async () => {
    page = await global.__BROWSER__.newPage();
    await page.emulate(devices['iPad landscape']);
    page.goto(mainUrl);

    await page.waitForSelector('input[name="pin"]');
    await page.type('input[name="pin"]', passcode, {delay: 30});

    await page.select('select[name="user_id"]', testRoomName);
    await page.waitForSelector('#activate', { visible: true });
    await page.tap('#activate');

    await page.waitForSelector('#btnYes', { visible: true });
    await page.tap('#btnYes');
  }, timeout);

  /**
   * Test #1: make sure a guest is logged into a room and able to see the homepage
   */
  test(`guest is logged into a room ${testRoomName}`, async () => {
    await page.waitForSelector('.thecolony-room-number');

    let text = await page.content();
    expect(text).toContain(`<div class="thecolony-room-number" data-current-user="${testRoomName}"></div>`);
  }, timeout);

  /**
   * Test #2: go to the Weather page and make sure weather container loads correctly
   */
  test('guest visits Weather page', async () => {
    await page.waitForSelector('.left-weather a');
    await page.tap('.left-weather a');
    await page.waitForSelector('.wu-simple-forecast');
    let text = await page.content();
    expect(text).toContain('wu-simple-forecast');
  }, timeout);

  afterAll(async () => {
    await page.close()
  });
});
